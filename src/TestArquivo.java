
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestArquivo {

    public static void main(String[] args) {

        Pessoa p = new Pessoa("Jose da Couves", "(27) 9999-9999");
        Pessoa p2 = new Pessoa("Silas", "(27) 9999-8888");
        Pessoa p3 = new Pessoa("Raphael", "(27) 9999-8888");
        Pessoa p4 = new Pessoa("Jonas", "(27) 9999-8888");
        Pessoa p5 = new Pessoa("Yuri", "(27) 9999-8888");
        Pessoa p6 = new Pessoa("Jhony", "(27) 9999-8888");
        Pessoa p7 = new Pessoa("Marcelo", "(27) 9999-8888");
        Pessoa p8 = new Pessoa("Samuel", "(27) 9999-8888");
        Pessoa p9 = new Pessoa("Dick", "(27) 9999-8888");
        Pessoa p0 = new Pessoa("Bruno", "(27) 9999-8888");
        Pessoa p11 = new Pessoa("Alex", "(27) 9999-8888");

        List<Pessoa> listaDePessoas = new ArrayList<>();
        listaDePessoas.add(p);
        listaDePessoas.add(p2);
        listaDePessoas.add(p3);
        listaDePessoas.add(p4);
        listaDePessoas.add(p5);
        listaDePessoas.add(p6);
        listaDePessoas.add(p7);
        listaDePessoas.add(p8);
        listaDePessoas.add(p9);
        listaDePessoas.add(p0);
        listaDePessoas.add(p11);
        File file = new File("contatos.txt");

        try {
            FileWriter fw;
            if (!file.exists()) {
                file.createNewFile();
                fw = new FileWriter(file);
            } else {
                fw = new FileWriter(file, true);
            }

            for (Pessoa x : listaDePessoas) {
                fw.write("Nome:" + x.getNome() + " Telefone:" + x.getTelefone() + "\n");
            }

            fw.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
